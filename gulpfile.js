var gulp = require('gulp');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var gulpCopy = require('gulp-copy');
var htmlmin = require('gulp-htmlmin');
var rename = require("gulp-rename");
var clean = require('gulp-dest-clean');

//minify build.index.html and then renames it to index.html
gulp.task('minifyHTML', function() {
    return gulp.src('build.index.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('build/'))
});

//copies all images
gulp.task('copyImages', function() {
    return gulp.src('images/**/*')
        .pipe(gulpCopy("build/"));
});

//copies rs-plugin folder
gulp.task('copyPlugin', function() {
    return gulp.src('rs-plugin/**/*')
        .pipe(gulpCopy("build/"));
});

//copies fonts folder
gulp.task('copyFont', function() {
    return gulp.src('fonts/**/*')
        .pipe(gulpCopy("build/"));
});

//copy E-mail file
gulp.task('copyEmailFile', function() {
    return gulp.src('email.php')
        .pipe(gulpCopy("build/"));
});

//minify all css and concat it into given order and builds on style.css file
gulp.task('minifyCss', function() {
    return gulp.src([
            'css/bootstrap.css',
            'css/jpreloader.css',
            'css/animate.css',
            'css/flexslider.css',
            'css/plugin.css',
            'css/prettyphoto.css',
            'css/owlcarousel.css',
            'css/owltheme.css',
            'css/main.css',
            'css/customstyle.css',
            'css/color.css',
            'css/snow.css'
        ])
        .pipe(cleanCSS())
        .pipe(concat("style.css"))
        .pipe(gulp.dest("build/css/"));
});

//uglify all raw js to build/js/extra folder
gulp.task('uglifyJs', function() {
    return gulp.src([
            'js/easing.js',
            'js/jquery.ui.totop.js',
            'js/jquery.scrollto.js',
            'js/owl.carousel.js',
            'js/jquery.countTo.js',
            'js/custom.js',
            'js/classie.js',
            'js/validation.js'
        ])
        .pipe(uglify())
        .pipe(gulp.dest("build/js/extra"));
});

//copies all js to build/js/extra folder
gulp.task('copyJs', function() {
    return gulp.src([
            'js/jquery.min.js',
            'js/jpreLoader.min.js',
            'js/bootstrap.min.js',
            'js/jquery.prettyPhoto.js',
            'js/jquery.isotope.min.js',
            'js/jquery.flexslider-min.js',
            'js/snowfall.jquery.min.js'
        ])
        .pipe(gulp.dest("build/js/extra"));
});

//run uglifyJs and copyJs tasks, then concat it all to all.js file and then cleans build/js/extra folder
gulp.task('concatJs', ['uglifyJs', 'copyJs'],  function() {
    return gulp.src([
            'build/js/extra/jquery.min.js',
            'build/js/extra/jpreLoader.min.js',
            'build/js/extra/bootstrap.min.js',
            'build/js/extra/jquery.isotope.min.js',
            'build/js/extra/jquery.prettyPhoto.js',
            'build/js/extra/easing.js',
            'build/js/extra/jquery.ui.totop.js',
            'build/js/extra/jquery.flexslider-min.js',
            'build/js/extra/jquery.scrollto.js',
            'build/js/extra/owl.carousel.js',
            'build/js/extra/jquery.countTo.js',
            'build/js/extra/classie.js',
            'build/js/extra/custom.js',
            'build/js/extra/validation.js',
            'build/js/extra/snowfall.jquery.min.js'
    ])
        .pipe(concat('all.js'))
        .pipe(clean('build/js', 'extras/**'))
        .pipe(gulp.dest("build/js/"));
});

// runs all tasks
gulp.task('default', ['copyImages' , 'copyPlugin', 'copyFont' , 'copyEmailFile' , 'minifyCss', 'concatJs','minifyHTML']);